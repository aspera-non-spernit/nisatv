require(["data", "nisa"], function(data, nisa) {
    $(document).ready(function() {
        var data = require("./data");
        var nisa = require("./nisa");
        var active = $(".schedule").attr("id") ; // + "-icon-link";
        var player = new nisa.VideoPlayer("#screen");
//        var matches = data.matches();
//        var m; // a match
        function resize() {
            if ( $(window).width() > $(window).height() ) {          
                var height = $(window).height() - $("#header").height() -$("#controls").height() - $("#footer").height();
                console.log("height" + height);
                $("#screen").css("height", height);
                $("#screen").css("width", height * 1.67);
            } 
            else if ( $(window).height() > $(window).width() ) {
                var width = $(window).width();          
                $("#screen").css("width", width);
                $("#screen").css("height", width / 1.67);
            }
        }
        function filter_matches(active) {
            var filtered = [];
            for (i = 0; i < matches.length; i++) {
                if (matches[i].home.short === active || matches[i].away.short === active) {
                    filtered.push(matches[i]);
                }
            }
            return filtered;
        }
        let matches;
	    function get_matches(query_string, callback) {
	        console.log("req " + query_string); 	
		    var xhr = new XMLHttpRequest();
		    xhr.open("GET", "http://localhost:8001/" + query_string, true);
		    xhr.onload = function (e) {
		      if (xhr.readyState === 4) {
		        if (xhr.status === 200) {
                    callback(JSON.parse(xhr.responseText));
		        } else {
		          console.error(xhr.statusText);
		        }
		      }
		    };
		    xhr.onerror = function (e) {
                console.error(xhr.statusText);
		    };
		    xhr.send(null);

	    }
	    
	    function sort_matches(matches) {
	        matches.sort(
                function(a, b) {
					return a.date - b.date
				}
			);
			for (var m in matches) {
                if (matches[m].date > date) {
					return matches[m];
	    		} 
            }
	    }
	    function play(matches) {
	        console.log(matches[0].date);
	        player.play(matches[0].broadcasts[0].video_link); 
            //console.log(sort_matches(matches));
	      //   var  m = player.prev_by_date(matches, new Date());
	     //    if (typeof m === "undefined") {
           //     console.log("no next match found");
             //   m = player.next_by_date(matches, new Date());
             //   if (typeof m === "undefined") {
               //     console.log("no matches found.");
               // } else {
                 //   if (m.broadcasts !== null && m.broadcasts.length != 0) {   

                        //player.play(m.broadcasts[0].video_link); 
                  //  } else {
                    //    console.log("no broadcasts found.");
                   // }
               //}
       //     }
	    }
	    if (active !== "index") {
	        var ms = get_matches("/matches?by_short=" + active, play);
	    } else {
	        var ms = get_matches("/all/matches", play);
	    }
	          
        resize();
      
/**        

        m = player.next_by_date(matches, new Date());
        if (typeof m === "undefined") {
            console.log("no next match found");
        } 
        m = player.prev_by_date(matches, new Date());
        if (typeof m === "undefined") {
            console.log("no matches found.");
        } else {
            if (m.broadcasts !== null && m.broadcasts.length != 0) {
                console.log(m.date);
                console.log(m.broadcasts.length);     
                player.play(m.broadcasts[0].video_link); 
            }
        }

**/


    }); // document ready
    
}); // require

/**
 
	    
	    var clubs = data.clubs();
        // avoid jumping controls due to resizing #player
        // doesn't work. needs display: hidden, is visibility
        $("#controls").removeClass("invisible");
        $("#controls").addClass("visible");
        $("#footer").removeClass("invisible");
        $("#footer").addClass("visible");
        $("#body").css("max_height", window.height);
   
        $(window).on("resize", function() {
	    console.log("RESIZING");
            resize();
        });
	console.log(active_short+"-icon-link");
   	$(active_short+"-icon-link").addClass("active");

// load current match
        var matches = data.matches();
        // player.next initialized after first next_by..
        var current_match = player.next_by_date(matches, new Date());
        if (typeof current_match === 'undefined') {
            current_match = player.prev_by_date(matches, new Date());
        }
        
        // loading takes a while
        if (current_match.broadcasts !== null) {
            player.play(current_match.broadcasts[0].video_link);
        }

        // binds click listener to element. here for the matches in the overlay / schedule
        function bind(elem) {
            $(elem).bind('click', function() {
                var i = $(this).attr('id').split("-")[2];
                $(".overlay").css("display", "none");
                var matches = data.matches();
                var active_icon = $("#" + $(this).attr('id').split('-')[3] + "-icon_link");
                deactivate_icon(active_icon);
                current_match = matches[i];
                player.play(current_match.broadcasts[0].video_link);
            });
        }
     
        var osd_on = false;
        function osd(text) { // single container element <div... to display
            if (osd_on === false && text !== null) {
                $(".overlay").empty();
                $(".overlay").append(text);
                $(".overlay").css("display", "block");
                osd_on = true;
            } else if (osd_on === true && text !== null) {
                $(".overlay").empty();
                $(".overlay").append(text);
            } else if (osd_on === true && text === null) {
                $(".overlay").empty();
                $(".overlay").css("display", "none");
                osd_on = false;
            }
        }
    }

        // prepares the schedule, does not display schedules
        function render_schedule(matches, filter) {
            console.log("matches" + matches);
            var schedule = schedule + '<div class="row">'; // left and right column

            schedule = schedule + '    <div class="col-lg-6">'; // left column
            schedule = schedule + '        <h3 class="text-white"><u>SCHEDULE</u></h3>';
                    // for all matches 
                    for (var m in matches) {
                        // guest or home team matches filter
                        if (matches[m].home.short === filter.short || matches[m].away.short === filter.short) {
                            schedule = schedule + '<div id="media-match-'+ m +'-' + filter.short +'" class="media">';
                            // // contender icon
                            if (matches[m].home.short === filter.short) {
                                schedule = schedule + '<img class="align-self-start mr-1" src="./assets/nisa_' + matches[m].away.short + '.png" title="'+ matches[m].away.name    +'">';
                                schedule = schedule + '<div id="media-body-'+ m +'" class="media-body">';
                                schedule = schedule + '<h5 class="mt-0 text-white">' + matches[m].away.name + '</h5>';
                            // playing away, show icon of home team
                            } else if (matches[m].away.short === filter.short) {
                                schedule = schedule + '<img class="align-self-start mr-1" src="./assets/nisa_' + matches[m].home.short + '.png"  title="'+ matches[m].home.name  +'">';
                                schedule = schedule + '<div id="media-body-'+ m +'" class="media-body">';
                                schedule = schedule + '<h5 class="mt-0 text-white">@' + matches[m].home.name + '</h5>';
                            }
                            // broadcast and attendence row
                            schedule = schedule + '<p>';
                            schedule = schedule + '<span id="match_date-'+ m + '" class="mt-0 text-white"><small>' + matches[m].date + '</small></span><br />';
                            // boradcast info
                            if (matches[m].broadcasts !== null && matches[m].broadcasts !== "undefined") {
                                schedule = schedule + '<span class="mt-0 text-info"><small><b>Currently no broadcast available.</b> </small></span>';
                                 if (matches[m].broadcasts[0].providers !== null && matches[m].broadcasts[0].providers !== "undefined") {
                                    schedule = schedule +
                                    '<span class="mt-0 text-info"><small><b>Commentators: </b>' + matches[m].broadcasts[0].providers + ' </small></span>';
                                }
                            }
                            // attendence
                            if (matches[m].attendence === null) {
                                schedule = schedule + '<span class="text-info"><small><b>Attendence:</b> n/a</small></span>';
                            } else if (matches[m].attendence !== null) {
                                schedule = schedule + '<span><small><a href="' + matches[m].attendence.source + '" target="_blank"><b>Attendence:</b> ' + matches[m].attendence.count + '</a></small></span>';
                            }
                            schedule = schedule + '</p>';

                            schedule = schedule + '</div>'; // media-body
                            schedule = schedule + '</div>'; // media

                        }
                    }
            schedule = schedule + '    </div>'; // left column
            if (filter.short === "dcfc") {
                schedule = schedule + '<div id="podcast" class="col-lg-6 text-white">'; // right column
                schedule = schedule + '    <h3 class="text-white"><u>DETROIT CITY RADIO PODCAST</u></h3>';
                schedule = schedule + '    <iframe width="100%" height="auto" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/690026119&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>';
                schedule = schedule + '</div>';
            }
            schedule = schedule + '</div>'; // row
            return schedule;
        }

        function render_standings() {
            return '<iframe id="standings" src="https://nisasoccer.com/standings" width="100%" height="100%" frameborder="0" allow="fullscreen" allowfullscreen></iframe>';
        }
       
        function resize() {
            if ( $(window).width() > $(window).height() ) {          
                var height = $(window).height() - $("#header").height() -$("#controls").height() - $("#footer").height();
    console.log("height" + height);
                $("#player").css("height", height);
                $("#player").css("width", height * 1.777);
            } 
            else if ( $(window).height() > $(window).width() ) {
                var width = $(window).width();          
                $("#player").css("width", width);
                $("#player").css("height", width / 1.777);
            }
        }
        // action listeners
        // control buttons prev next 
        // TODO: player
        $("#prev_btn").click(function() { 
	    console.log("PREV");
            current_match = player.prev_by_date(data.matches(), current_match.date);

            if (current_match.broadcasts !== null || current_match.broadcasts !== "undefined" ) {
                console.log("current " + current_match.home.short + ": " + current_match.away.short);
                player.play(current_match.broadcasts[0].video_link); 
            }
        });
        $("#next_btn").click(function() {
            current_match = player.next_by_date(data.matches(), current_match.date);
            if (current_match.broadcasts[0] !== null) {
                player.play(current_match.broadcasts[0].video_link);
            }
        });
        
        // click listener for the club menu icons
        $('[id$="-icon_link"]').click(function() {
            var club_filter = clubs;
           
            if (this.id !== "nisa-icon_link") {

                club_filter = club_filter.find(c => c.short === $(this).attr("id").split("-")[0]);
                console.log("CLICKED: " + club_filter);
            } 
            // turn on filter
            if (filter_active === null) {
                activate_icon(this, "0.3em solid white");
                if (this.id != $("#nisa-icon_link").attr("id") ) {
                    osd( render_schedule(data.matches(), club_filter) );
                    var match_elems = $("[id^=media-match-]");
                   // bind after osd appened elements
                    for (var i = 0; i < match_elems.length; i++ ) {
                        bind($('[id="' + match_elems[i].id +'"]'));
                    }
                } else if (this.id === $("#nisa-icon_link").attr("id")) {
                    osd( render_standings() );
                }
                filter_active = this;
            // turn off filter if clicked same icon twice
            } else if (filter_active !== null && this === filter_active) {
                // deactivate and unbind currently active
                deactivate_icon(this);
                var match_elems = $("[id^=media-match-]");
                for (var i = 0; i < match_elems.length; i++ ) {
                    $('[id="' + match_elems[i].id +'"]').unbind("click");
                }
                osd(null);
                filter_active = null;
            // switch filter if different icon clicked
            } else if (filter_active !== null && this !== filter_active) {
                // deactivate and unbind currently active
                deactivate_icon(filter_active);
                var match_elems = $("[id^=media-match-]");
                for (var i = 0; i < match_elems.length; i++ ) {
                    $('[id="' + match_elems[i].id +'"]').unbind("click");
                }
                // activate and bind new one
                activate_icon(this, "0.3em solid white");
                filter_active = this;
                if (this.id !== $("#nisa-icon_link").attr("id")) {
                    osd( render_schedule(data.matches(), club_filter) );   
                    //bind after osd appened elements
                    var match_elems = $("[id^=media-match-]");
                   // bind after osd appened elements
                    for (var i = 0; i < match_elems.length; i++ ) {
                        bind($('[id="' + match_elems[i].id +'"]'));
                    }
                } else if (this.id === $("#nisa-icon_link").attr("id")) {
                    osd( render_standings() );
                }
            }
        });
    });
**/
