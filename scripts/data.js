define(["require", "nisa"], function(require, nisa) 
{
    var nisa = require("./nisa");
    function clubs()  {
        var c = [
            new nisa.Club(1, "pf", "Philadelphia Fury", null),
            new nisa.Club(2, "sa", "Stumptown Athletic", null),
            new nisa.Club(3, "sdfc", "1904 FC San Diego", null),
            new nisa.Club(4, "laf", "LA Force", null),
            new nisa.Club(5, "dcfc", "Detroit City FC", new nisa.Colors("#800000", "#c3a500", "#000")),
            new nisa.Club(6, "mia", "Miami FC", null),
            new nisa.Club(7, "mich", "Michigan Stars", null),
            new nisa.Club(8, "nycc", "New York City Cosmos", null),
            new nisa.Club(9, "orsc", "Oakland Roots SC", null),
            new nisa.Club(10, "atl", "Atlanta SC", null),
            new nisa.Club(11, "cus", "California United Strikers", null),
            new nisa.Club(12, "cfc", "Chattanooga FC", null),
            new nisa.Club(99, "tba", "To be announced", null)
        ];
        return c;
    }
    
    return {
        clubs: function() {
            return clubs();
        },
        matches: function()
        {
            var pf = clubs().find( (c => c.short == "pf") );
            var sa = clubs().find( (c => c.short == "sa") );
            var sdfc = clubs().find( (c => c.short == "sdfc") );
            var laf = clubs().find( (c => c.short == "laf") );
            var dcfc = clubs().find( (c => c.short == "dcfc") );
            var mia = clubs().find( (c => c.short == "mia") );
            var mich = clubs().find( (c => c.short == "mich") );
            var nycc = clubs().find( (c => c.short == "nycc") );
            var orsc = clubs().find( (c => c.short == "orsc") );
            var atl = clubs().find( (c => c.short == "atl") );
            var cus = clubs().find( (c => c.short == "cus") );
            var cfc = clubs().find( (c => c.short == "cfc") );                                                
            return new Array(
                // 2020
                // Spring Season
                new nisa.Match(
                    202001, laf, dcfc, new Date(2010, 01, 28, 19, 5), null,
                    [
                        new nisa.Broadcast(26, "", "https://mycujoo.tv/embed/2399?id=90212&shareSource=EM_EM")
                    ]
                ),
                // 2019
                // Championships
                new nisa.Match(201931, cus, laf, new Date(2019, 10, 10, 19, 5), null, null),
                new nisa.Match(201930, mia, sa, new Date(2019, 10, 9, 19, 0), null,
                    [
                        new nisa.Broadcast(25, "", "https://mycujoo.tv/embed/1939?id=83320&shareSource=EM_EM")
                    ]
                ),
                
                new nisa.Match(201929, sdfc, laf, new Date(2019, 10, 2, 22, 5), null,
                    [
                        new nisa.Broadcast(23, "Simon Allen, Alex Naveja", "https://mycujoo.tv/embed/2328?id=74200&shareSource=EM_EM"
                    )
                ]),
                // Regular
                // Week 9
                new nisa.Match(201927, laf, cus, new Date(2019, 9, 26, 22, 2), null, null), new nisa
                .Match(201925, mia, orsc, new Date(2019, 9, 26, 19, 0), null, [new nisa
                    .Broadcast(21, "tba", "https://mycujoo.tv/embed/1939?id=74195&shareSource=EM_EM")
                ]), new nisa.Match(201924, sa, atl, new Date(2019, 9, 25, 19, 0), null, [new nisa
                    .Broadcast(20, "Jason Hanover", "https://mycujoo.tv/embed/2326?id=74193&shareSource=EM_EM"
                    )
                ]),
                // Week 8
                new nisa.Match(201923, cus, sdfc, new Date(2019, 9, 20, 19, 0), null, [new nisa
                    .Broadcast(19, "tba", "https://mycujoo.tv/embed/846?id=74189&shareSource=EM_EM")
                ]),
                
                new nisa.Match(201921, orsc, laf, new Date(2019, 9, 19, 19, 0), [new nisa.Attendence(
                    5237, "https://twitter.com/oaklandrootssc/status/1185771140880326658?s=21"
                ), new nisa.Broadcast(18, "Charles Wollin, Stephen Davies", "https://mycujoo.tv/embed/2327?id=74199&shareSource=EM_EM"
                )]),
                
                // Week 7
                new nisa.Match(201919, sa, cfc, new Date(2019, 9, 12, 19, 1), null, [new nisa
                    .Broadcast(17, "tba", "https://mycujoo.tv/embed/2326?id=74192&shareSource=EM_EM")
                ]), new nisa.Match(201918, atl, mia, new Date(2019, 9, 12, 19, 0), null, [new nisa
                    .Broadcast(16, "Jason Hanover", "https://mycujoo.tv/embed/429?id=74196&shareSource=EM_EM"
                    )
                ]),
                // Week 6
                new nisa.Match(201917, mia, atl, new Date(2019, 9, 6, 19, 0), null,
                    [
                        new nisa.Broadcast( 15, "tba", "https://mycujoo.tv/embed/1939?id=74194&shareSource=EM_EM")
                    ]
                ),
                
                new nisa.Match(201930, cfc, dcfc, new Date(2019, 9, 5, 19, 0), null,
                    [
                    new nisa.Broadcast(25, "tba", "https://www.youtube.com/embed/DKskL4I2QU8"
                    )
                ]),
                new nisa.Match(201914, cus, sdfc,
                    new Date(2019, 9, 2, 22, 0),
                    new nisa.Attendence( 1102, "https://www.calunitedstrikers.com/news/2019/10/03/cal-united-strikers-v-1904fc-game-recap"),
                    [
                        new nisa.Broadcast(14, "Simon Allen, Alex Naveja", "https://mycujoo.tv/embed/846?id=74188&shareSource=EM_EM")
                    ]
                ),
                // Week 5
                
                new nisa.Match(201911, sdfc, orsc, new Date(2019, 8, 28, 22, 4),
                    new nisa.Attendence(2536, "https://www.reddit.com/user/LeonardFC"), 
                    [
                        new nisa.Broadcast(10,
                            "Simon Allen, Alex Naveja",
                            "https://mycujoo.tv/embed/2328?id=74167&shareSource=EM_EM"
                        )
                    ]
                ),
                new nisa.Match(201910, cus, orsc, new Date(2019, 8, 25, 10, 0),
                    new nisa.Attendence(1025, "https://www.calunitedstrikers.com/news/2019/09/26/california-united-strikers-fc-v-oakland-roots-game-re-cap"),
                    [
                        new nisa.Broadcast(9, "Simon Allen, Alex Naveja", "https://mycujoo.tv/embed/846?id=74170&shareSource=EM_EM")
                    ]
                ),
                // Week 4
                new nisa.Match(201909, mia, sa, new Date(2019, 8, 22, 19, 2), new nisa.Attendence(312,
                    "https://www.reddit.com/user/LeonardFC"),
                    [
                        new nisa.Broadcast(8, "n/a", "https://mycujoo.tv/embed/1939?id=74164&shareSource=EM_EM")
                    ]
                ),
                new nisa.Match(201907, cus, laf, new Date(2019, 8, 22, 19, 1),
                    new nisa.Attendence(2092, "https://www.calunitedstrikers.com/news/2019/09/23/california-united-strikers-la-force-game-recap"),
                    [
                        new nisa.Broadcast(7, "Simon Allen, Alex Naveja", "https://mycujoo.tv/embed/846?id=74168&shareSource=EM_EM")
                    ]
                ),
                // Week 3
                new nisa.Match(201905, mia, pf, new Date(2019, 8, 15, 19, 1), new nisa.Attendence(728,
                    "https://www.reddit.com/user/LeonardFC"), [new nisa
                    .Broadcast(5, "n/a", "https://mycujoo.tv/embed/1939?id=74163&shareSource=EM_EM")
                ]),
                new nisa.Match(201904, atl, sa, new Date(2019, 8, 15, 19, 0), new nisa.Attendence(112,
                    "https://www.reddit.com/user/LeonardFC"), [new nisa
                    .Broadcast(4, "Jason Hannover", "https://mycujoo.tv/embed/429?id=74161&shareSource=EM_EM")
                ]),
                new nisa.Match(201903, sdfc, cus, new Date(2019, 8, 14, 22, 4), new nisa.Attendence(2838,
                    "https://www.calunitedstrikers.com/news/2019/09/16/california-united-strikers-fc-vs-1904fc-game-recap-"
                ), [new nisa.Broadcast(3, "Simon Allen, Gustavo Ortega",
                    "https://mycujoo.tv/embed/2328?id=74166&shareSource=EM_EM"
                )]),
                // Week 2
                new nisa.Match(201903, laf, sdfc, new Date(2019, 8, 7, 22, 0), null, [new nisa.Broadcast(2, "n/a",
                    "https://mycujoo.tv/embed/2399?id=75102&shareSource=EM_EM"
                )]),
                // Week 1
                new nisa.Match(201902, orsc, cus, new Date(2019, 7, 31, 22, 0), null, [new nisa.Broadcast(1,
                    "Charles Wollin, Stephen Davies",
                    "https://mycujoo.tv/embed/2327?id=73910&shareSource=EM_EM"
                )]),
                new nisa.Match(201901, dcfc, pf, new Date(2019, 7, 31, 19, 30), new nisa.Attendence(5958,
                    "https://www.detcityfc.com/news_article/show/1045797"), new nisa.Broadcast(0,
                    "Neil Ruhl, John Krieger", "https://mycujoo.tv/embed/1845?id=73386&shareSource=EM_EM"
                ))
            );
        }
    }
    }
);
